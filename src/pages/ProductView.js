import { useState, useEffect, useContext, Component } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function CourseView() {

	const { productId } = useParams();
	const { user } = useContext(UserContext);
	const history = useHistory();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	class inputPage extends Component {
		state ={
			value: 0,
		};

		decrease = () => {
			this.setState({value: this.state.value - 1})
		};

		increase = () => {
			this.setState({ value: this.state.value + 1})
		};
	}

	const addToCart = (productId) => {

		fetch(`https://hidden-ridge-45141.herokuapp.com/api/users/checkout`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if (data === true) {

				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: "You have successfully added this to cart."
				});

				history.push("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		});

	};

	useEffect(()=> {
		console.log(productId);
		fetch(`https://hidden-ridge-45141.herokuapp.com/api/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});

	}, [productId]);


	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<div class="container">
						<Card>
							<Card.Header className="text-center">{name}</Card.Header>
							<Card.Body className="text-left">
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>PhP {price}</Card.Text>
								<Card.Subtitle>Quantity</Card.Subtitle>
								<Card.Text>1</Card.Text>
								{ user.id !== null ? 
										<Button variant="primary" block onClick={() => addToCart(productId)}>Add to cart</Button>
									: 
										<Link className="btn btn-danger btn-block" to="/login">Log in to purchase</Link>
								}
							</Card.Body>
						</Card>
					</div>
				</Col>
			</Row>
		</Container>
	)
}