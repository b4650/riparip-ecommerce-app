import { Fragment, useEffect, useState, useContext } from 'react';
import AdminView from '../components/AdminView';

import UserView from '../components/UserView';
import UserContext from '../userContext';

export default function Products() {
    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    const fetchData = () => {

        fetch(`https://hidden-ridge-45141.herokuapp.com/api/products/`)
        .then(res => res.json())
        .then(data => {
            setProducts(data);
        });

    }

    useEffect(() => {
        fetchData();
    }, []);
    
    return (
        <Fragment>
            {(user.isAdmin === true)
                ? <AdminView productsData={products} fetchData={fetchData}/>
                : <UserView productsData={products}/>
            }
        </Fragment>
    )

}