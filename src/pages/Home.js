import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

document.body.style = 'background: #cf70a6;';

export default function Home() {

	const data = {
	    title: "Zuitt Coding Bootcamp",
	    content: "Opportunities for everyone, everywhere",
	    destination: "/courses",
	    label: "Enroll now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}