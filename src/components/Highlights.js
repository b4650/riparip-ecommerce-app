import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            <center>
            <h2 xs={12} md={4}>Check out our top sellers!</h2>
            </center>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src="./images/1050ti.JPG" />
                    <Card.Body>
                        <Card.Title>
                            <h2>GTX 1050 TI</h2>
                        </Card.Title>
                        <Card.Text>
                            The GeForce GTX 1050 Ti is a mid-range graphics card by NVIDIA, launched on October 25th, 2016. Built on the 14 nm process, and based on the GP107 graphics processor, in its GP107-400-A1 variant, the card supports DirectX 12. This ensures that all modern games will run on GeForce GTX 1050 Ti. The GP107 graphics processor is an average sized chip with a die area of 132 mm² and 3,300 million transistors.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src="./images/1650.JPG" />
                    <Card.Body>
                        <Card.Title>
                            <h2>GTX 1650</h2>
                        </Card.Title>
                        <Card.Text>
                            The GTX 1650 uses a new TU117 GPU, which is a smaller and thus less expensive variant of the TU116 that powers the GTX 1660 and 1660 Ti cards. The key differences relative to the 1660 line are in the memory configuration and number of SMs (Streaming Multiprocessors), which in turn determines the number of CUDA cores, texture units, and ROPs.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Img variant="top" src="./images/1660super.JPG" />
                    <Card.Body>
                        <Card.Title>
                            <h2>GTX 1660 Super</h2>
                        </Card.Title>
                        <Card.Text>
                            The GeForce GTX 1660 SUPER is up to 20% faster than the original GTX 1660 and up to 1.5X faster than the previous-generation GTX 1060 6GB. Powered by the award-winning NVIDIA Turing™ architecture and ultra-fast GDDR6 memory, it’s a supercharger for today’s most popular games. Time to gear up and get SUPER.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
} 