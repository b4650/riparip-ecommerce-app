import {Component} from 'react'
import { Button, Row, Col } from 'react-bootstrap';

export default class Banner extends Component {
    render() {
        return (
            <Row>
                <img src="./images/Banner.jpg" className="banner" alt="" />
            </Row>
        )
    }
}