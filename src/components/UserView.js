import { Fragment, useState, useEffect } from 'react'
import ProductCard from "./ProductCard";

export default function UserView({productsData}) {
    const [products, setProducts] = useState([]);
    useEffect(() => {
        const productArr = productsData.map(product => {
        	if(product.isActive === true){
				return (
					<ProductCard productProp={product} key={product._id}/>
				)
        	}else{
        		return null;
        	}
        });
        setProducts(productArr);
    }, [productsData]);

    return(
        <Fragment>
            {products}
        </Fragment>
    );
}
