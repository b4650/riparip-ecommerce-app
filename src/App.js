import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './userContext';

function App() {
    const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

    const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		});
    };

    useEffect(() => {
		fetch(`https://hidden-ridge-45141.herokuapp.com/api/users/details`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (typeof data._id !== "undefined") {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				});
			} else {

				setUser({
					id: null,
					isAdmin: null
				});
			}
		})
    }, []);

	return (
		<UserProvider value={{user, setUser, unsetUser}}>
			<Router>
				<AppNavbar />
				<Container>
					<Switch>
						<Route exact path="/" component={Home}/>
						<Route exact path="/products" component={Products}/>
						<Route exact path="/products/:productId" component={ProductView}/>
						<Route exact path="/login" component={Login}/>
						<Route exact path="/logout" component={Logout}/>
						<Route exact path="/register" component={Register}/>
						<Route component={Error} />
					</Switch>
				</Container>
			</Router>
		</UserProvider>
	);
}

export default App;